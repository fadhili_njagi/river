import os, re

class music:
  def __init__(self):
    self.music_dir = os.path.expanduser("~") + "\\music"
  
  def scan_recursively(self, baseDir):
    for entry in os.scandir(baseDir):
      if entry.is_file():
        yield entry.path
      else:
        yield from self.scan_recursively(entry.path)
  
  def get_track_by_filter(self, filter):
    audio_file_exts = ['.mp3', '.m4a', '.wav', '.flac', '.wma', '.aac', '.ogg']
    selected_music = []

    for file in self.scan_recursively(self.music_dir):
      if os.path.splitext(file)[-1].lower() in audio_file_exts:
        if re.search(fr".*{'.*'.join(filter.split(' '))}.*", os.path.splitext(file)[-2].lower(), re.IGNORECASE):
          selected_music.append(file)
    
    return selected_music