"""
  Get latest news
  
  latest_news = news()
  latest_news.read_aloud(limit=5)
"""
import requests, bs4, os, sys

class news:
  def __init__(self, settings):
    self.settings = settings.getAttr("news")

  def get_news(self, category="general", limit=None):
    """ Get current news articles from Google News.

        Parameters:
          category (string): News Category to use. This is used to get the URL from the settings file. Default is general.
          limit (int | None): Limit to n articles.

        Returns:
          news (dict[ ]): Article title, source and date
    """
    # Obtaining news from source
    result = requests.get(self.settings[category])
    result.raise_for_status()
    # Parsing news using beautiful soup
    soup = bs4.BeautifulSoup(result.text, 'html.parser')
    # Get news titles
    articles = soup.find_all('div', class_="NiLAwe")
    
    news = []
    for article in articles[:limit]:
      news_item = {}
      # Extract title
      title_n_link = article.find('h3', class_="ipQwMb").find('a', class_="DY5T1d")
      title = title_n_link.text
      # Regulate title length
      if len(title) > 150:
        title = title[:150] + "..."
      news_item['title'] = title
      news_item['article_link'] = "https://news.google.com" + title_n_link['href'][1:]
      # Extract article link
      article.find('h3', class_="ipQwMb").find('a', class_="DY5T1d").text
      # Extract date and source
      metadata = article.find('div', class_="QmrVtf")
      news_item['source'] = metadata.find('a', class_="wEwyrc").text
      news_item['date'] = metadata.find('time', class_="WW6dff").text
      try:
        news_item['image'] = article.find('img', class_="tvs3Id")["src"]
      except:
        if os.path.exists("assets/news.jpg"):
          news_item['image'] = "assets/news.jpg"
        else:
          news_item['image'] = sys._MEIPASS + "/assets/news.jpg"

      # Extract coverage (context) link if existent
      coverage = article.find('a', class_="FKF6mc")
      if (coverage):
        news_item['coverage'] = "https://news.google.com" + coverage['href'][1:]
      # Append to news list
      news.append(news_item)
    return news