from kivy.logger import Logger, LOG_LEVELS
import logging, sys
# Set up logging
Logger.setLevel(LOG_LEVELS["warning"])
handler = logging.FileHandler('debug.log')
Logger.addHandler(handler)
sys.excepthook = lambda type, value, tb: Logger.error("[-] Uncaught exception", exc_info=(type, value, tb))

from kivy.config import Config
Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
Config.set('graphics', 'minimum_width', '600')
Config.set('graphics', 'minimum_height', '600')
Config.set('graphics', 'width', '600')

from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivymd.uix.card import MDCard
from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.label import MDLabel
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDRaisedButton
from kivy.properties import ObjectProperty, StringProperty
import json, weakref, threading, webbrowser, os
import mpt.assistantcore as core
from mpt.tcolors import tcolors
from kivy.core.audio import SoundLoader
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.resources import resource_add_path, resource_find

try:
  Builder.load_file("assistant.kv")
except:
  Builder.load_file(sys._MEIPASS + "/assistant.kv")

Window.clearcolor = (1, 1, 1, 1)

class ChatMessage(MDLabel):
  text = StringProperty('')

class ChatMessageSent(ChatMessage):
  text = StringProperty('')
  pass

class Listening(Image):
  pass

class AppCard(MDCard):
  name = StringProperty('')
  path = StringProperty('')
  
  def open_app(self, app, name):
    try:
      os.system(f'"{app}"')
    except:
      MDApp.get_running_app().show_error_dialog(f"Error opening '{name}'")


class NewsArticle(MDCard):
  image = StringProperty('')
  title = StringProperty('')
  date = StringProperty('')
  link = StringProperty('')
  source = StringProperty('')

  def goto(self, link):
    webbrowser.open(link)

class MyLayout(Widget, core.Assistant):
  input_field = ObjectProperty(None)
  input_mode = False

  def respond(self, response):
    """
    Assistant sending response to console, UI and speaker
    """
    self.ids.chatbox.add_widget(ChatMessage(text=response))
    self.add_to_history({
      "type": "msg",
      "sent": 0,
      "text": response
    })
    x = threading.Thread(target=self.tts.print_say, args=(response,), daemon=True)
    x.start()
    x.join()

  def disable_input(self):
    """
    Schedule disabling of inputs
    """
    Clock.schedule_once(lambda dt: self.disable_input_exec(), -1)

  def disable_input_exec(self):
    """
      Disable voice and text inputs
    """
    self.input_mode = True
    self.ids.send.disabled = True
    self.ids.speak.disabled = True
    self.ids.scroll.scroll_y = 0
  
  def enable_input(self):
    """
    Schedule enabling of inputs
    """
    Clock.schedule_once(lambda dt: self.enable_input_exec(), -1)

  def enable_input_exec(self):
    """
      Enable voice and text inputs
    """
    self.ids.speak.disabled = False
    self.ids.send.disabled = False
    self.input_mode = False
  
  def voice_input(self, *args):
    """
      Get voice input
    """
    # Disable input
    self.disable_input()

    # Listen for voice command
    listening_anim = Listening()
    threading.Thread(target=self.play_listening_sound, daemon=True).start()
    self.ids.chatbox.add_widget(listening_anim)
    self.ids['listening_gif'] = weakref.proxy(listening_anim)
    print(f"\n{tcolors.BOLD}[+]{tcolors.ENDC} Starting listening thread")
    threading.Thread(target=self.listen, daemon=True).start()

  def listen(self):
    inp = ''
    count = 0
    while count < 2:
      print(f"{tcolors.OKBLUE}Listening...{tcolors.ENDC}")
      inp = self.voice_to_text()

      if inp:
        self.ids.chatbox.remove_widget(self.ids["listening_gif"])
        self.ids.pop('listening_gif', None)
        self.new_command(inp)
        self.enable_input()
        return
      
      print(f"You just said '{inp}'")
      count += 1
    self.ids.chatbox.remove_widget(self.ids["listening_gif"])
    self.enable_input()
  
  def text_input(self, inp):
    self.ids.input_field.text = ""
    threading.Thread(target=self.text_cmd, args=(inp,), daemon=True).start()
  
  def text_cmd(self, inp):
    if inp:
      self.disable_input()
      self.new_command(inp)
      self.enable_input()
  
  def new_command(self, cmd):
    print("Creating widget")
    self.ids.chatbox.add_widget(ChatMessageSent(text=cmd.capitalize()))
    self.add_to_history({
      "type": "msg",
      "sent": 1,
      "text": cmd.capitalize()
    })
    print("Executing instruction")
    self.exec_instruction(cmd)
  
  def add_to_history(self, dict):
    with open(MDApp.get_running_app().chat_history_file, "r") as data:
      history = json.load(data)

    history.append(dict)
    with open(MDApp.get_running_app().chat_history_file, "w") as f:
      f.write(json.dumps(history))
  
  def add_news_article(self, article, *args):
    self.ids.chatbox.add_widget(NewsArticle(title=article["title"], source=article['source'], date=article['date'], link=article["article_link"], image=article["image"]))
    article["type"] = "news"
    self.add_to_history(article)
  
  def add_app_card(self, name, path, mode=1, *args):
    self.ids.chatbox.add_widget(AppCard(name=name, path=path))
    self.add_to_history({"type": "app", "name": name, "path": path})
    # Open app if mode is 1
    if mode == 1:
      threading.Thread(target=self.open_app, args=(path, name,), daemon=True).start()
  
  def add_disambiguation_links(self, links, *args):
    self.ids.chatbox.add_widget(ChatMessage(text=links))
    self.add_to_history({"type": "msg", "sent": 0, "text": links})

class MyApp(MDApp):
  title = "River"
  intros = True
  playing_track = ""

  def build(self):
    self.layout = MyLayout()
    history = []
    if os.path.exists("chat_history.json"):
      self.chat_history_file = "chat_history.json"
    else:
      self.chat_history_file = sys._MEIPASS + "/chat_history.json"
    # Fetch chat history
    with open(self.chat_history_file, "r") as data:
      history = json.load(data)

    # Display chat history
    for item in history:
      if item["type"] == "msg":
        if (item["sent"] == 0):
          new_msg = ChatMessage(text=item["text"])
        elif (item["sent"] == 1):
          new_msg = ChatMessageSent(text=item["text"])
        self.layout.ids.chatbox.add_widget(new_msg)
      elif item["type"] == "news":
        if "image" not in item.keys():
          item["image"] = "news.jpg"
        self.layout.ids.chatbox.add_widget(NewsArticle(title=item["title"], source=item['source'], date=item['date'], link=item["article_link"], image=item["image"]))
      elif item["type"] == "app":
        self.layout.ids.chatbox.add_widget(AppCard(name=item['name'], path=item['path']))
    
    if len(history):
      self.intros = False

    # Load listening sound
    if os.path.exists("assets/when-604.mp3"):
      self.listening_sound = SoundLoader.load("assets/when-604.mp3")
    else:
      self.listening_sound = SoundLoader.load(sys._MEIPASS + "/assets/when-604.mp3")
    
    return self.layout
  
  def on_start(self):
    print("Starting")
    threading.Thread(target=self.on_open, daemon=True).start()
  
  def on_open(self):
    self.layout.disable_input()
    if self.intros:
      self.layout.whoami()
      self.layout.hint()
    else:
      self.layout.first_greeting()
    self.layout.enable_input()
  
  def show_error_dialog(self, msg):
    self.dialog = MDDialog(
      title=msg,
      size_hint=(None, None),
      width=300,
      buttons = [
        MDRaisedButton(text='OK', on_press=self.dismiss_dialog)
      ]
    )
    self.dialog.open()
  
  def dismiss_dialog(self, *args):
    self.dialog.dismiss()


if __name__ == '__main__':
  if hasattr(sys, '_MEIPASS'):
    resource_add_path(os.path.join(sys._MEIPASS))
  # if getattr(sys, 'frozen', False):
  #   # this is a Pyinstaller bundle
  #   resource_add_path(sys._MEIPASS)
  MyApp().run()