# Platorm module to identify OS
from os import error
import platform
from mpt.tcolors import tcolors

class tts:
  def __init__(self, voice_id=0, rate=150, volume=1):
    # Windows - use pyttsx3 for text to speech
    if platform.system() == "Windows":
      import pyttsx3
      engine = pyttsx3.init()
      voices = engine.getProperty('voices')
      engine.setProperty('voice', voices[voice_id].id)
      engine.setProperty('rate', rate)
      engine.setProperty('volume', volume)
      self.engine = engine
    # For MAC or Linux, use gtts for text to speech
    elif platform.system() == "Darwin" or platform.system() == "Linux":
      import os
      self.rate = "" if rate >= 135 else " --slow "

  def print_say(self, text: str):
    if platform.system() == "Windows":
      print(f"\n{tcolors.BOLD}[River]{tcolors.ENDC}: {text}")
      self.engine.say(text)
      self.engine.runAndWait()
    elif platform.system() == "Darwin" or platform.system() == "Linux":
      print(text)
      text = text.replace('"', '')
      text = text.replace("'", "")
      os.system(f'gtts-cli --nocheck{self.rate} "{text}" | mpg123 -q -')