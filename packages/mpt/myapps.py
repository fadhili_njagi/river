import os, re

class apps:
  def scan_recursively(self, *dirs):
    for baseDir in dirs:
      for entry in os.scandir(baseDir):
        if entry.is_file():
          yield entry.path
        else:
          yield from self.scan_recursively(entry.path)

  def get_apps_by_filter(self, filter):
    apps_location = [
      os.path.join(os.environ["userprofile"], "AppData\Roaming\Microsoft\Windows\Start Menu\Programs"),
      os.path.join(os.environ["userprofile"], "desktop"),
      r"C:\Users\Public\Desktop"
    ]
    selected_app_paths = []
    selected_app_names = []

    for file in self.scan_recursively(*apps_location):
      (filename, ext) = os.path.splitext(file)
      filename = filename.split("\\")[-1]
      if ext.lower() == ".lnk":
        if re.search(f".*{'.*'.join(filter.split(' '))}.*", filename.lower(), re.IGNORECASE):
          if filename not in selected_app_names:
            selected_app_paths.append(file)
            selected_app_names.append(filename)
  
    # Choose exact match if present, in the case of multiple matches
    if len(selected_app_names) > 1:
      for i in range(len(selected_app_names)):
        if filter.lower() == selected_app_names[i].lower():
          return [[selected_app_names[i]], [selected_app_paths[i]]]
    
    return [selected_app_names, selected_app_paths]
