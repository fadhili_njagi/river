from .mysr import voice_to_text
from .mysay import tts
from .mynews import news
from .mymusic import music
from .myapps import apps
from .settings import settings
from .tcolors import tcolors
from .assistantcore import Assistant