import threading, time, random, os, re, datetime, webbrowser, wolframalpha, wikipedia, requests
from mpt.mysay import tts
from mpt import mysr, mymusic, myapps
from mpt.mynews import news
from mpt.settings import settings
from kivymd.app import MDApp
from kivy.clock import Clock
from functools import partial

class Assistant:
  input_mode = False

  def __init__(self):
    self.tts = tts()
    self.settings = settings()
    self.news = news(self.settings)
    threading.Thread(target=self.listen_in_background, daemon=True).start()
  
  def respond(self, response):
    """ Overriden by child class """
    pass

  def listen(self):
    """ Overriden by child class """
    pass

  def voice_input(self):
    """ Overriden by child class """
    pass

  def add_news_article(self, article):
    """ Overriden by child class """
    pass

  def add_app_card(self, name, path, mode):
    """ Overriden by child class """
  
  def add_disambiguation_links(self, links):
    """ Overriden by child class """
    pass
  
  def voice_to_text(self, timeout=None):
    """ Convert speech to text using Google's TTS engine """
    return mysr.voice_to_text(timeout=timeout)

  def exec_instruction(self, inp: str):
    global _tts
    inp = inp.lower()

    # Greetings
    if inp in ["hi", "hola", "hello", "hey"]:
      self.respond(random.choice(["Hello", "Hey", "What's up", "Salve. That's hello in Italian", "Ola", "Konnichiwa. That's hello in Japanese"]))
    elif inp in ["how are you", "how are you doing"]:
      self.respond("I'm great. I would be even happier doing things for you")
    
    # Exit
    elif inp == "exit":
      self.respond("Exiting")
      MDApp.get_running_app().stop()
    
    # Shutdown
    elif inp == "shutdown":
      self.respond("Shutting down PC")
      os.system('shutdown /s')
    # Hibernate
    elif inp == "hibernate":
      self.respond("Hibernating PC")
      os.system('shutdown /h')
    # Restart
    elif inp == "restart":
      self.respond("Restarting PC")
      os.system('shutdown /r')
    # Sign out
    elif inp == "sign out":
      self.respond("Signing out")
      os.system('shutdown /l')
    
    # Time
    elif inp in ["what's the time", "what is the time"]:
      curr_time = datetime.datetime.now().strftime("%I:%M %p")
      self.respond(f"It is now {curr_time}")
    
    # Date
    elif inp in ["what's the date today", "what is the date today"]:
      curr_date = datetime.datetime.now().strftime("%A, %B %d, %Y")
      self.respond(f"Today is {curr_date}")
    
    # Google search
    elif inp.startswith("search "):
      query = inp[7:]
      self.respond(f"Opening Google search in the browser")
      webbrowser.open(f"https://google.com/search?q={query}")

    # Youtube search
    elif inp.startswith("youtube "):
      query = inp[8:]
      self.respond(f"Opening Youtube search in the browser")
      webbrowser.open(f"https://www.youtube.com/results?search_query={query}")
    
    # Open url
    elif inp.startswith("open url "):
      filter = inp[9:]
      if webbrowser.open(f"http://{filter}"):
        self.respond(f"Opening '{filter}' in the browser")
      else:
        self.respond(f"I could not open the url '{filter}'")
    
    # Live radio
    elif inp in ["radio", "open radio"]:
      webbrowser.open("https://onlineradiobox.com/ke")
      self.respond("Opening a live radio website on your browser")
    
    # Apps
    elif inp.startswith("open "):
      filter = inp[5:]
      apps = myapps.apps()
      (app_name, app_path) = apps.get_apps_by_filter(filter)
      if len(app_name) == 0:
        self.respond(f"I could not find '{filter.capitalize()}'")
      elif len(app_name) == 1:
        self.respond(f"Opening {app_name[0]}")
        Clock.schedule_once(partial(self.add_app_card, app_name[0], app_path[0], 1), -1)
      elif len(app_name) > 1:
        self.respond("I found several apps. Please select one below")
        for i in range(len(app_name)):
          Clock.schedule_once(partial(self.add_app_card, app_name[i], app_path[i], 0), -1)
    
    # Music
    elif inp.startswith("play "):
      self.get_music(inp[5:])
    
    # Cross-category news summary
    elif inp in ["what's the latest news", "what is the latest news", "what's on the news", "what is on the news", "read me the news", "give me a news summary"]:
      categories = ["general", "local", "business", "technology", "entertainment", "sports", "science", "health", "covid"]
      self.respond(f"Here is a summary of the top 3 articles on every category from Google news")
      for item in categories:
        self.respond(f"On {item.capitalize()} news")
        self.get_news(category=item, limit=3)
    
    # Category specific news summary
    elif len(re.findall("read me the (general|local|business|technology|entertainment|sports|science|health|covid) news", inp)) > 0:
      category = re.findall("read me the (general|local|business|technology|entertainment|sports|science|health|covid) news", inp)[0]
      self.respond(f"Here is a summary of the top 5 articles on {category} category from Google News")
      self.get_news(category=category, limit=5)
    
    # Joke
    elif inp in ["tell me a joke", "make me laugh"]:
      self.random_joke()

    # General Queries
    else:
      # Search wolframaplha API
      wolf = wolframalpha.Client(self.settings.getAttr("api_keys")["wolframalpha"])
      try:
        res = wolf.query(inp)
        answer = next(res.results).text
        self.respond(answer)
      except:
        # Fallback to Wikipedia
        try:
          answer = wikipedia.summary(inp, sentences=5)
          self.respond("According to Wikipedia")
          self.respond(answer)
        except wikipedia.exceptions.DisambiguationError as e:
          self.respond(f"I found a disambiguation page. The options are listed below")
          list = ""
          for i in range(len(e.options)):
            list += f"\n{i + 1}. {e.options[i]}"
          Clock.schedule_once(partial(self.add_disambiguation_links, list[1:]), -1)
            
        except wikipedia.exceptions.PageError:
          self.respond("I don't know the answer to that yet, but I'm learning")
        except:
          self.respond(f"Sorry, I'm having trouble connecting to the internet")
  
  def listen_in_background(self):
    while True:
      try:
        if self.input_mode == False:
          inp = self.voice_to_text(timeout=5)
          print(f"You just said '{inp}'")
          if inp in ["hello river", "river", "hey river", "computer", "hi computer", "hey computer", "ok computer", "ok river", "assistant"]:
            # Call voice input on the main thread
            Clock.schedule_once(self.voice_input, -1) 

      except mysr.sr.WaitTimeoutError:
        pass

      time.sleep(.2)
  
  def get_news(self, category="general", limit=1):
    news = self.news.get_news(category=category, limit=limit)
    index = 1
    for item in news:
      # Schedule adding news article to UI
      Clock.schedule_once(partial(self.add_news_article, item), -1)
      # Printing to console and reading out aloud
      speech = f"Article {index}: \n{item['title']} \nBy {item['source']}, {item['date']}"
      self.tts.print_say(speech)
      print("-"*80)
      index += 1
  
  def get_music(self, filter):
    music = mymusic.music()
    tracks = music.get_track_by_filter(filter)
    if len(tracks) == 0:
      self.respond(f"I could not find music matching the criteria '{filter}' on your computer")
    elif len(tracks) == 1:
      self.respond(f"Playing '{filter}'")
      self.play_music(tracks[0])
    elif len(tracks) > 1:
      self.respond(f"I found several tracks.\nChoosing one at random")
      self.play_music(random.choice(tracks))
  
  def play_music(self, path):
    os.startfile(path)
  
  def play_listening_sound(self):
    MDApp.get_running_app().listening_sound.play()
  
  def open_app(self, path, name):
    print("[+] Opening app")
    try:
      os.system(f'"{path}"')
    except:
      self.respond(f"I could not open {name}")
  
  def random_joke(self):
    joke = requests.get("https://v2.jokeapi.dev/joke/Any").json()
    if joke["type"] == "single":
      self.respond(joke['joke'])
    else:
      self.respond(joke['setup'])
      time.sleep(1)
      self.respond(joke['delivery'])
  
  def first_greeting(self, *args):
    hour = datetime.datetime.now().hour
    greeting = "Good Evening"
    if hour >= 0 and hour < 12:
      greeting = "Good Morning"
    elif hour >= 12 and hour < 18:
      greeting = "Good Afternoon"
    self.respond(greeting)
    self.respond(random.choice(["How can I help?", "What can I do for you today?", "What do you want me to do?"]))
  
  def whoami(self, *args):
    self.respond("Hello, I'm River, your virtual assistant")
    self.respond("You can address me as River or Computer")
    self.respond("I can do tasks and retrieve information for you")
  
  def hint(self, *args):
    self.respond('Try saying "tell me a joke", or "what\'s the latest news"')
