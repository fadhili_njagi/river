import json, os, sys

class settings:
  """ Get settings from settings.json file """

  def __init__(self):
    """ Load settings from settings.json file """
    if os.path.exists("settings.json"):
      settings_file = "settings.json"
    else:
      settings_file = sys._MEIPASS + "/settings.json"
    with open(settings_file, "r") as data:
      self.settings = json.load(data)
  
  def getAttr(self, attr):
    return self.settings[attr]